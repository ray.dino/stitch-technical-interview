# Usage

## Start the script

`python script.py`


## Available commands

`GET <key>`
Retrieves the value with the associated key

`SET <key> <value>`
Sets the value for the given key

`UNSET <key>`
Removes the associated key and value

`NUMEQUALTO <value>`
Counts how many keys have the given value

`BEGIN`
Begins a transaction

`COMMIT`
Ends the transaction while saving the changes

`ROLLBACK`
Ends the transaction while discarding the changes
 
`END`
Exits the program


## Run tests

`python tests.py`
