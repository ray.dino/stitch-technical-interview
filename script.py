from store import Store
	

def main():
	store = Store()

	while True:
		cmd = input().split()

		if cmd[0] in store.commands.keys():
			try:
				result = store.commands[cmd[0]](*cmd[1:])
				print(result)
			except Exception as e: 
				# This acts as a simple validation for if the input has more arguments than needed
				print(e)
		else:
			print('error')


if __name__ == '__main__':
	main()