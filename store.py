class Store:

	def __init__(self):
		"""
			For creating a key-value store, a python dictionary is the most appropriate.
		"""
		self.store = {}
		self.backup_store = {}
		self.commands = {
			'GET': self.get,
			'SET': self.set_kv,
			'UNSET': self.unset,
			'NUMEQUALTO': self.numequalto,
			'BEGIN': self.begin,
			'COMMIT': self.commit,
			'ROLLBACK': self.rollback,
			'END': self.end
		}

	"""Retrieves the value with the associated key

	O(1) time complexity
	"""
	def get(self, key):
		if self.store.get(key):
			return self.store[key]
		else:
			return('NULL')

	"""Sets the value for the given key

	Assumption: If a key already exists, we just want to overwrite it.
	O(1) time complexity
	"""
	def set_kv(self, key, value):
		self.store[key] = value
		return('')

	"""Removes the associated key and value

	Assumption: We don't want to know if a key exists before deletion.
	O(1) time complexity
	"""
	def unset(self, key):
		if self.store.get(key):
			del self.store[key]
		return('')

	"""Counts how many keys have the given value

	O(n) time complexity
	"""
	def numequalto(self, value):
		count = sum(map(lambda x: 1 if x == value else 0, self.store.values()))
		return(count)

	"""Begins a transaction
	
	Will fail if a transaction is already in progress.

	Here we use a copy of the key-value store to capture the current state.
	The current store will be used for the transaction while the backup will be used for rolling back.

	We compromised here to achieve a simpler solution at the cost of 
	space (maintaining a copy of the whole dictionary) and
	time (copying the dictionary requires iterating through it)

	O(n) time complexity
	"""
	def begin(self):
		if len(self.backup_store):
			return('TRANSACTION IN PROGRESS')
		else:
			self.backup_store = self.store.copy()
			return('')

	"""Ends the transaction while saving the changes

	Since the current store has the changes from the transaction,
	we simple throw away the backup since we don't need it anymore.
	
	O(1) time complexity
	"""
	def commit(self):
		if len(self.backup_store):
			self.backup_store = {}
			return('')
		else:
			return('NO TRANSCATION')

	"""Ends the transaction while discarding the changes

	We reset the current store using the backup, 
	then we discard the backup.
	
	O(1) time complexity
	"""
	def rollback(self):
		if len(self.backup_store):
			self.store = self.backup_store
			self.backup_store = {}
			return('')
		else:
			return('NO TRANSACTION')

	def end(self):
		exit()