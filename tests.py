import unittest
from store import Store


class TestStore(unittest.TestCase):

	store = Store()

	def setUp(self):
		self.store.set_kv('x',1)

	def test_set_and_get(self):
		self.assertEqual(self.store.get('x'), 1)
	
	def test_unset(self):
		self.store.unset('x')
		self.assertEqual(self.store.get('x'), 'NULL')
	
	def test_numequalto(self):
		self.store.set_kv('y',1)
		self.assertEqual(self.store.numequalto(1), 2)
	
	def test_transaction_commit(self):
		self.store.begin()
		self.store.set_kv('y', 2)
		self.store.commit()
		self.assertEqual(self.store.get('y'), 2)
	
	def test_transaction_rollback(self):
		self.store.begin()
		self.store.set_kv('z', 3)
		self.store.rollback()
		self.assertEqual(self.store.get('z'), 'NULL')


if __name__ == '__main__':
	unittest.main()